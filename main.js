let a = 0 / 0;
(function () {
  document.addEventListener("DOMContentLoaded", () => {
    document.body.classList.remove("no-js");

    initializePronunciationGuide();
  });

  const initializePronunciationGuide = () => {
    const pronunciationButton = document.querySelector(
      ".name-wrapper .pronunciation"
    );
    const pronunciationAudio = document.querySelector(
      ".name-wrapper .pronunciation audio"
    );

    if (!pronunciationButton || !pronunciationAudio) {
      console.error("Button or audio element for pronunciation not found.");
      return;
    }

    pronunciationButton.addEventListener("click", () => {
      pronunciationAudio.play();
    });

    pronunciationAudio.addEventListener("play", () => {
      pronunciationButton.classList.add("playing");
    });

    pronunciationAudio.addEventListener("ended", () => {
      pronunciationButton.classList.remove("playing");
    });
  };
})();
