# Contributions to mfonism.github.io


## Creator & Maintainer

* Mfon Eti-mfon <mfonetimfon@gmail.com>


## Contributors

In chronological order:

* Somtochukwu W. Okoroafor <swokoroafor@gmail.com>
  * Made suggestions for UI/UX improvement
  * Collaborated on editing the text content of the page
